package by.EPAM.trainJava;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

/***
 * 
 * @author Sergii_Kotov
 * работа с пользователем через консоль
 * Представление напрямую нигде не использует модель
 * Модель нигде напрямую не использует представление 
 */
public class ConsoleView {
	static Scanner scanner = new Scanner(System.in);
	private static final String errMsg = "Server hasn't been reached or smth else have been going wrong...";
	ControllerStore ctrlS;
	/***
	 * консольное приложение умеет в поиск, ввод и еще немножко команд
	 */
	public enum ViewActions {
		WAIT,FINDBYTAG,FINDBYCONTENT,ADD,FINDBYGROUP,DEFINEGROUP,SETSORTING,exit
		}
	//private ViewActions state;
	//Set <String> ViewActions = new HashSet <String>(Arrays.asList("Wait","FindByTag","FindByContent","Add","FindByGroup","DefineGroup","SetSorting"));
	
	public ConsoleView (){
		//state=ViewActions.WAIT;
		//state.ordinal();
		//System.out.println(state);
		ctrlS= new ControllerStore();
		startServer();
	}
	
	public String allActions() {
		String s="";
		//
		for (ViewActions g: ViewActions.values()) {
			s+=g.name() +" ("+g.ordinal()+") | ";
		}
		return s; //Arrays.toString(ViewActions.values());
	}
	
	public String getAction() {
		String s="";
		if(scanner.hasNext()) {
			s= scanner.next();
		}	
		ViewActions a;
		try {
			 a=ViewActions.valueOf(s);
		    } catch (IllegalArgumentException ex) {
		    	a=null;
		}
		 
		if (a!=null){
			doAction(a);
		} else { //дублирование команд ввода порядковым номером команды. для скорости ввода.
			int codeAction;
			try { 
				codeAction=Integer.parseInt(s);
				for (ViewActions g: ViewActions.values()) {
					if (codeAction==g.ordinal()) {
						s=doAction(g);
					}
				}
			} catch (NumberFormatException e) {
			}
		}	
		return s;
	}
	
	public void printInfo(String mes) {
		System.out.print(mes);
	}

	/***
	 *  вспомогательный метод конверсии строки в календарь
	 * @param sDF
	 * @return
	 */
	private Calendar convertToCalendar (String sDF){
		DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		Date date;
		Calendar calendar = Calendar.getInstance();
		try {
			date = formatter.parse(sDF);
		} catch (ParseException e) {
			date=null;
			System.out.print("It is not correct date format");
		}
		calendar.setTime(date);
		return calendar ;
	}
	/***
	 * for manual input
	 * @param a what to do
	 * @return action result
	 */
	private String doAction(ViewActions a) {
		String s="";
		if (a==ViewActions.WAIT){
			
		} else {
			if (a==ViewActions.exit){
				s=a.toString();
			} else {
				if (a==ViewActions.FINDBYTAG){
					System.out.print("choose tag:\n 1.Name\n2. Category\n3. Code_Producer\n4. Date factory\n5. Annotation");
					int r=-1;
					if(scanner.hasNextInt()) {
						r= scanner.nextInt();
					}else {
						scanner.next();
					}
					System.out.print("Enter search text");
					if(scanner.hasNext()) {
						s= scanner.next();
					}
					if (r==2) { 
						findTag(r,Integer.valueOf(s));
					} else {
						if (r==3) {
							Calendar calendar = convertToCalendar(s);
							findTag(r,calendar);
						} else {
							findTag(r,s);
						}
					}
					
					findTag(r,s);
				}
				if (a==ViewActions.ADD){
					String sNm="";
					System.out.print("Enter value for Name");
					if(scanner.hasNext()) {
						sNm= scanner.next();
					}
					String sCat="";
					System.out.print("Enter value for Category");
					if(scanner.hasNext()) {
						sCat= scanner.next();
					}
					Integer sCP=-1;
					System.out.print("Enter value for Code_Producer");
					if(scanner.hasNextInt()) {
						sCP= scanner.nextInt();
					} else {
						scanner.next();
					}
					String sDF="";
					System.out.print("Enter value for Date factory in MM/dd/yy format");
					if(scanner.hasNext()) {
						sDF= scanner.next();
					}
					Calendar calendar = convertToCalendar(sDF);
					String sAn="";
					System.out.print("Enter value for Annotation");
					if(scanner.hasNext()) {
						sAn= scanner.next();
					}
					addItem(sNm,sCat,sCP,calendar,sAn);
				}
			}
		}
		return s;
	}
	
	public void addItem(String nm, String sCat,Integer sCP,Calendar sDF,String sAn) {
		try {
			System.out.println(ctrlS.addNewItem(nm, sCat,sCP,sDF,sAn));
		} catch (ExceptionDuplicateGood e) {
			System.out.println("Такой товар уже есть!");
		}
		catch (Exception e) {
			System.out.println(errMsg);
		}
	}
	
	public void findTag(int r,Object s) {
		System.out.println("Waiting for server response...");
		try {
			System.out.println("server has found next items: "+ctrlS.findByField(r,s));
		} catch (Exception e) {
			System.out.println(errMsg);
		}
	}

	public void findTagGroup(Map <Integer, Object> hM) {
		System.out.println("Waiting for server response...");
		try {
			System.out.println("server has found next items: "+ctrlS.findByFieldGroup(hM));
		} catch (Exception e) {
			System.out.println(errMsg);
		}
	}
	public void startServer() {
		System.out.println("Starting...");
		try {
			if (ctrlS.start()) {
				System.out.println("already has been started");
			} else {
				System.out.println("started succesfully");
			}
		} catch (Exception e) {
			System.out.println("Something is wrong with server. It was not started properly");
		}
	}
	
	public void closeServer() {
		System.out.println("Closing...");
		try {
			if (ctrlS.close()) {
				System.out.println("already has been closed");
			} else {
				System.out.println("closed succesfully");
			}
		} catch (Exception e) {
			System.out.println("Something is wrong with server. It was not closed properly");
		}
	}
	
}
