package by.EPAM.trainJava;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class GoodsList implements Serializable {
	private static final long serialVersionUID = 1L;

	/***
	 * множество товаров на складе это сет. 
	 * Не должно быть повторяющихся товаров. 
	 */
	private Set<Good> stGood;
	
	public GoodsList() {
		stGood=new HashSet <Good>();
	}

	public GoodsList(Set <Good> stGood) {
		this.stGood = stGood;
	}

	public void add(Good g) {
		if (stGood.contains(g)) {
			throw new ExceptionDuplicateGood();
		}
		stGood.add(g);
	}
	
	/***
	 * поиск по одному тегу
	 * есть чудесный метод compareSet у товара который по айди поля проверяет его значение
	 * вот и используем его. 
	 * но вся сила его будет видна при сравнении группы 
	 * @param fldN по какому полю 
	 * @param t строка или число или дата поиска
	 * @return подсписок всех товаров с найденными товарами. отсортирован дефолтной сортировкой
	 */
	public String find (int fldN, Object t) {
		boolean smthFound=false;
		Set<Good> resL = new HashSet<Good>();
		
		Map <Integer, Object> hM = new HashMap<Integer,Object> ();
		Good.addToMap(hM,fldN,t);
		for (Good g: stGood){
			if (g.compareSet(hM)){
				resL.add(g);
				smthFound=true;
			}
		}
		if (smthFound) {
			return sortByName(resL).toString();
		} else {
			return "... nothing was found...";
		}
			
	}

	/***
	 * когда ищем по группе полей товара просто указываем для каждого поля 
	 * айди поля в котором ищем и поисковое значение которое м.б. строкой, датой или числом
	 * 
	 * @param hM	Map где ключ - код поля, значение - искомое значение.
	 * @return	результат отсортирован дефолтной сортировкой
	 */
	public String findGroup (Map <Integer, Object> hM) {
		boolean smthFound=false;
		Set<Good> resL = new HashSet<Good>();
		// передадим правильный тип
		for (Good g: stGood){
			if (g.compareSet(hM)){
				resL.add(g);
				smthFound=true;
			}
		}
		if (smthFound) {
			return sortByName(resL).toString();
		} else {
			return "... nothing found...";
		}
	}
	
	public Set<Good> getStGood() {
		return stGood;
	}

	public void setLstGood(Set<Good> lstGood) {
		stGood = lstGood;
	}

	public int getSize() {
		return stGood.size();
	}
	
    private List<Good> sortByName(Set<Good> stGood) {
    	List<Good> lst=new ArrayList <Good> ();
    	lst.addAll(stGood);
        Collections.sort(lst, new CompareBaseByName());
        return lst;
    }
	
	@Override
	public String toString() {
		StringBuilder r= new StringBuilder ();
		r.append(String.format("%25s%25s%25s%35s%35s\n","name","catName","codeProducer","dateFactory","annotation"));
		for (Good g: stGood) {
			r.append(g);
		}
		return r.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stGood == null) ? 0 : stGood.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GoodsList other = (GoodsList) obj;
		if (stGood == null) {
			if (other.stGood != null)
				return false;
		} else if (!stGood.equals(other.stGood))
			return false;
		return true;
	}
	
	

}
