package by.EPAM.trainJava;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Map;
/***
 * 
 * @author Sergii_Kotov
 * Модель нигде напрямую не использует представление 
 * Представление напрямую нигде не использует модель
 */
public class FileModel {
	GoodsList gl;
	
	FileModel() {
		gl = new GoodsList();
	}
	
	/***
	 * поискать на складе по полю
	 * @param fld код поля
	 * @param val строка поиска
	 */
	public String findByField (int fld, Object val) {
		return gl.find(fld, val);
	}

	/***
	 * поискать на складе по группе полей
	 */
	public String findByFieldGroup (Map <Integer, Object> hM)  {
		return gl.findGroup(hM);
	}

	/**
	 * десериализация при закрытии
	 * @param fileName сериализуем из файла с указанным именем
	 * @return
	 * @throws Exception
	 */
	public boolean start (String fileName) throws ExceptionFileStore { 
		// read from file
	    File f = new File(fileName);
	    if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileInputStream fi= new FileInputStream(fileName);
            ObjectInputStream oi= new ObjectInputStream(fi);
            GoodsList gl2=(GoodsList) oi.readObject();
            if (gl2.getSize()>0) {
            	gl = gl2;
            }
            oi.close();
        } catch (IOException | ClassNotFoundException e) {
            try {
                if (gl == null) {
                    gl = new GoodsList();
                }
            } catch (Exception e1) {
                throw new ExceptionFileStore(e1); 
            }
        }
		return true;
	}

	/**
	 * сериализация при закрытии
	 * @param fileName сериализуем в файл с указанным именем
	 * @return
	 * @throws Exception
	 */
	public boolean close (String fileName) throws ExceptionFileStore {
		//write to file
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
	        ObjectOutputStream serialO = new ObjectOutputStream(fos);
	        serialO.writeObject(gl);
	        serialO.close();
		} catch (Exception e) {
			throw new ExceptionFileStore(e);
		}
		return true;
	}

	/***
	 * внесение в список товара
	 * @param sCat каталог
	 * @param sCP код производителя
	 * @param aDF дата производства
	 * @param sAn аннотация
	 * @return сведения о внесенном товаре
	 */
	public String addNewItem(String nm, String sCat,Integer sCP,Calendar aDF,String sAn) { //throws Exception
		// создание товара
		// добавление в список
		// вывод товара в результат
		// вывод новой длины списка
		Good g=new Good (nm, sCat,sCP,aDF, sAn);
		gl.add(g);
		//return gl.toString();
		return "added "+g+"There are "+gl.getSize()+" different goods in the store";
	}
	
}
