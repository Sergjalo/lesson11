package by.EPAM.trainJava;

import java.util.Map;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;


public class Good implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
    private String catName;
    private Integer codeProducer;
    private Calendar dateFactory;
    private String annotation;
    
    //public static final List <String> fields = Arrays.asList("name", "catName", "codeProducer","dateFactory","annotation");
    
    /**
     * для быстрого поиска будем хранить хеш коды
     */
    private int nameH;
    private int catNameH;
    private int codeProducerH;
    private int dateFactoryH;
    private int annotationH;

    
	public Good(String name, String catName, Integer codeProducer, Calendar dateFactory, String annotation) {
		this.name = name;
		this.catName = catName;
		this.codeProducer = codeProducer;
		this.dateFactory = dateFactory;
		this.annotation = annotation;
		this.nameH = (name == null) ? 0 : name.hashCode();
		this.catNameH = (catName == null) ? 0 : catName.hashCode();
		this.codeProducerH = (codeProducer == null) ? 0 : codeProducer.hashCode();
		this.dateFactoryH = (dateFactory == null) ? 0 : dateFactory.hashCode();
		this.annotationH = (annotation == null) ? 0 : annotation.hashCode();
	}

	/***
	 * подготовка для сравнения по группе полей 
	 * это по-сути просто утилитный метод. пусть будет статическим
	 * @param hM
	 * @param fldN
	 * @param t
	 */
	public static void addToMap (Map <Integer, Object> hM, int fldN, Object t) {
		if (hM==null) {
			hM = new HashMap<Integer,Object> ();
		}
		// передадим правильный тип
		if (t instanceof String) {
			hM.put(fldN,(String)t);
		} else {
			if (t instanceof Calendar) {
				hM.put(fldN,(Calendar)t);
			} else {
				if (t instanceof Integer) {
					hM.put(fldN,(Integer)t);
				}
			}
		}
	}

	// сравниваем по любому набору полей
	public boolean compareSet (Map <Integer,Object> hashMap ) { //= new HashMap<String,Object> ()
		boolean isEqual=true;
		for (Map.Entry<Integer, Object>  e: hashMap.entrySet()) {
			if (e.getKey()==1) { //name
				// при сравнении сначала смотрим хеши и если они не совпадают то по значениям и не сравниваем
				//String dS=(String)e.getValue();
				//int hc =dS.hashCode();
				if ((e.getValue().hashCode()!=nameH)||(!name.equals((String)e.getValue()))){
					isEqual=false;
					break;
				}
			}
			if (e.getKey()==2) { //catName
				if ((e.getValue().hashCode()!=catNameH)||(!catName.equals((String)e.getValue()))){
					isEqual=false;
					break;
				}
			}
			if (e.getKey()==3) { //codeProducer
				if ((e.getValue().hashCode()!=codeProducerH)||(!codeProducer.equals((String)e.getValue()))){
					isEqual=false;
					break;
				}
			}
			if (e.getKey()==4) { //dateFactory
				if ((e.getValue().hashCode()!=dateFactoryH)||(!dateFactory.equals((String)e.getValue()))){
					isEqual=false;
					break;
				}
			}
			if (e.getKey()==5) { //annotation
				if ((e.getValue().hashCode()!=annotationH)||(!annotation.equals((String)e.getValue()))){
					isEqual=false;
					break;
				}
			}
		}
		return isEqual;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.nameH = (name == null) ? 0 : name.hashCode();
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
		this.catNameH = (catName == null) ? 0 : catName.hashCode();
	}

	public Integer getCodeProducer() {
		return codeProducer;
	}

	public void setCodeProducer(Integer codeProducer) {
		this.codeProducer = codeProducer;
		this.codeProducerH = (codeProducer == null) ? 0 : codeProducer.hashCode();
	}

	public Calendar getDateFactory() {
		return dateFactory;
	}

	public void setDateFactory(Calendar dateFactory) {
		this.dateFactory = dateFactory;
		this.dateFactoryH = (dateFactory == null) ? 0 : dateFactory.hashCode();
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
		this.annotationH = (annotation == null) ? 0 : annotation.hashCode();
	}

	@Override
	public String toString() {
		return String.format("%25s%25s%25s%35s%35s\n",name,catName,codeProducer,dateFactory.getTime(),annotation);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + catNameH;
		result = prime * result + codeProducerH;
		result = prime * result + nameH;
		result = prime * result + annotationH;
		result = prime * result + dateFactoryH;
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Good other = (Good) obj;
		if (annotation == null) {
			if (other.annotation != null)
				return false;
		} else if (!annotation.equals(other.annotation))
			return false;
		if (catName == null) {
			if (other.catName != null)
				return false;
		} else if (!catName.equals(other.catName))
			return false;
		if (codeProducer == null) {
			if (other.codeProducer != null)
				return false;
		} else if (!codeProducer.equals(other.codeProducer))
			return false;
		if (dateFactory == null) {
			if (other.dateFactory != null)
				return false;
		} else if (!dateFactory.equals(other.dateFactory))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    
	
	
}
