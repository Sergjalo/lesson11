package by.EPAM.trainJava;

import java.util.Calendar;
import java.util.Map;

/***
 * 
 * @author Sergii_Kotov
 * взаимодествие модели и представления только через контроллер
 */
public class ControllerStore {
	FileModel fm;
	
	ControllerStore(){
		fm=new FileModel();	
	}

	public boolean start () throws ExceptionController {
		boolean b=false; 
		try { 
			b=fm.start ("e:\\fileLesson12.store");
		} catch (Exception e){
			// выдаем исключение контроллера вместо внутреннего исключения
			throw new ExceptionController(e); 
		}
		return b; 
	}

	public boolean close () throws ExceptionController {
		boolean b=false; 
		try { 
			b=fm.close ("e:\\fileLesson12.store");
		} catch (Exception e){
			// выдаем исключение контроллера вместо внутреннего исключения
			throw new ExceptionController(e); 
		}
		return b; 
	}
	
	/***
	 * запросить у модели результаты поиска по полю
	 * @param fld код поля
	 * @param val строка поиска
	 */
	public String findByField (int fld, Object val) throws ExceptionController {
		String s="";
		try {
			s= fm.findByField(fld, val);
		} catch (Exception e){
			// выдаем исключение контроллера вместо внутреннего исключения
			throw new ExceptionController(e); 
		}
		return s;
	}
	
	public String findByFieldGroup (Map <Integer, Object> hM)  throws ExceptionController{
		String s="";
		try {
			s= fm.findByFieldGroup(hM);
		} catch (Exception e){
			// выдаем исключение контроллера вместо внутреннего исключения
			throw new ExceptionController(e); 
		}
		return s;
	}
	
	public String addNewItem(String nm,String sCat,Integer sCP,Calendar aDF,String sAn)  throws ExceptionController {
		String s="";
		try {
			s= fm.addNewItem(nm,sCat,sCP,aDF,sAn);
		} catch (ExceptionDuplicateGood e){ 
			//пока пробросим то же исключение, оно информативно и на уровне пользователя 
			throw new ExceptionDuplicateGood(e); 
		}
		return s;	
	}
	
	
}
