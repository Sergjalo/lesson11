package by.EPAM.trainJava;

import java.util.Comparator;

public class CompareBaseByName implements Comparator<Good> {
    public int compare(Good o1, Good o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}
