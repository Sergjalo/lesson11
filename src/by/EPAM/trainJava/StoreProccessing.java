package by.EPAM.trainJava;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/***
 * 
 * @author Sergii_Kotov
 * запуск приложения
 */
public class StoreProccessing {
	public static void main(String[] args) throws ParseException {
		ConsoleView cv = new ConsoleView ();
		cv.printInfo(cv.allActions());
		cv.printInfo("\nadd item\n");

		DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		Date date;
		Calendar calendar = Calendar.getInstance();
		date = formatter.parse("01/01/2017");
		calendar.setTime(date);

		cv.addItem("Wool sport socks","Socks", 11223, calendar, "Cool socks. For real men.");
		cv.addItem("Wool warm socks","Socks", 11223, calendar, "Cool socks. For real women.");
		cv.findTag(1, "Wool sport socks"); // найдет искомое
		cv.findTag(1, "Wool2 sport socks"); // ничего не найдет
		
		cv.printInfo("\nпоиск по группе\n");
		// этот метод, моя прелесть, в консоль не выносил, только отсюда можно пробовать
		Map <Integer, Object> hM = new HashMap<Integer,Object> ();
		Good.addToMap(hM,1,"Wool sport socks");
		Good.addToMap(hM,5,"Cool socks. For real men.");
		cv.findTagGroup(hM);

		cv.printInfo("\nеще один поиск по группе\n"); // не должен ничего найти - имя не совпадает, хотя второй тег и совпадает.
		Good.addToMap(hM,1,"Wool2222 sport socks");
		Good.addToMap(hM,5,"Cool socks. For real men.");
		cv.findTagGroup(hM);

		cv.addItem("Simple socks","Socks", 11, calendar, "No description");
		cv.addItem("Simple warm socks","Socks", 11, calendar, "No description");
		cv.findTag(2, "Socks");
		
		// можно поиграться с ручным вводом или сразу нажать 7 и выйти
		cv.printInfo("enter code: ");
		String s=cv.getAction();
		while (!s.equals("exit")) {
			cv.printInfo("enter code: ");
			s=cv.getAction();
		}
		cv.printInfo("\nend");

		cv.closeServer();
		
	}
}
